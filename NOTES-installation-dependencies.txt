ftc
---
ftc requires the following modules, in order to run
in the host natively (that is, outside the Fuego docker
container).  These are python modules are not installed
by default in Ubuntu 18.04.

python-requests

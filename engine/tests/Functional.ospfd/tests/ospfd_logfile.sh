#!/bin/sh

#  In the target start ospfd and zebra, then confirm the log file.
#  check the /var/log/quagga/ospfd.log file.

test="logfile"

. ./fuego_board_function_lib.sh

set_init_manager

exec_service_on_target ospfd stop
exec_service_on_target zebra stop

if [ ! -d /var/log/quagga ]
then
   mkdir /var/log/quagga
   chown -R quagga:quagga /var/log/quagga
fi

if [ -f /var/log/quagga/ospfd.log ]
then
   mv /var/log/quagga/ospfd.log /var/log/quagga/ospfd.log.bck
fi

#Backup the config file
mv /etc/quagga/ospfd.conf /etc/quagga/ospfd.conf.bck
mv /etc/quagga/zebra.conf /etc/quagga/zebra.conf.bck

cp data/ospfd.conf /etc/quagga/ospfd.conf
cp data/zebra.conf /etc/quagga/zebra.conf
chown quagga:quagga /etc/quagga/*.conf

if exec_service_on_target zebra start
then
    echo " -> start of zebra succeeded."
else
    echo " -> start of zebra failed."
    echo " -> $test: TEST-FAIL"
    exit
fi

if exec_service_on_target ospfd start
then
    echo " -> start of ospfd succeeded."
else
    echo " -> start of ospfd failed."
    echo " -> $test: TEST-FAIL"
    exit
fi

if ls /var/log/quagga/ospfd.log
then
    echo " -> get log file of ospfd."
    echo " -> $test: TEST-PASS"
else
    echo " -> can't get log file of ospfd."
    echo " -> $test: TEST-FAIL"
    exit
fi

exec_service_on_target ospfd stop
exec_service_on_target zebra stop

#Restore the config file
mv /etc/quagga/ospfd.conf.bck /etc/quagga/ospfd.conf
mv /etc/quagga/zebra.conf.bck /etc/quagga/zebra.conf

if [ -f /var/log/quagga/ospfd.log.bck ]
then
    mv /var/log/quagga/ospfd.log.bck /var/log/quagga/ospfd.log
fi

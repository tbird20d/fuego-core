#!/usr/bin/python

import os, re, sys

sys.path.insert(0, os.environ['FUEGO_CORE'] + '/engine/scripts/parser')
import common as plib


regex_string = "^\ *[\d]{1,3}: *([\d]{1,8}) bytes .*? -->\ *([\d.]{1,8}) Mbps"

measurements = {}
matches = plib.parse_log(regex_string)

def set_measure(size, throughput):
    tguid = "default.size-"+size
    measurements[tguid] = [{"name": "throughput", "measure": float(throughput)}]

# grab only a few of the throughput values
size_list=["1024","8192","262144","4194304"]
if matches:
    for match in matches:
        size = match[0]
        if size in size_list:
            set_measure(size, match[1])


sys.exit(plib.process(measurements))

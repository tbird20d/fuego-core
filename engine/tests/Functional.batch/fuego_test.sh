NEED_ROOT=1

function test_pre_check {
    assert_has_program batch
    assert_has_program expect
}

function test_deploy {
    put $TEST_HOME/batch.sh $BOARD_TESTDIR/fuego.$TESTDIR/
    put -r $TEST_HOME/tests $BOARD_TESTDIR/fuego.$TESTDIR/
}

function test_run {
    report "cd $BOARD_TESTDIR/fuego.$TESTDIR;\
    sh -v batch.sh"
}

function test_processing {
    log_compare "$TESTDIR" "1" "TEST-PASS" "p"
}

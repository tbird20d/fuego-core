#!/bin/sh

# In target, run comannd batch and execute "echo" in batch
# Ensure that batch can work well.

test="batch"

if /tmp/batch_result
then
    rm /tmp/batch_result
fi

expect <<-EOF
spawn batch
expect {
 -re ".*at.*" {
           send_user " -> $test: batch executed.\n"
           send "echo batch test done > /tmp/batch_result\n"
          }
 default { send_user " -> $test: TEST-FAIL\n" }
 }
send "\004\r"
send_user " -> $test: batch executed succeeded.\n"
expect eof
EOF

sleep 5

if cat /tmp/batch_result | grep "batch test done"
then
    echo ' -> $test: TEST-PASS'
else
    echo ' -> $test: TEST-FAIL'
fi


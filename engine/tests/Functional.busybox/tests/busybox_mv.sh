#!/bin/sh

#  The testscript checks the following options of the command mv
#  1) Option none

test="mv"

mkdir test_dir
touch test_dir/test1
if [ "$(ls test_dir)" = "test1" ]
then
    echo " -> $test: ls test_dir succeeded."
else
    echo " -> $test: ls test_dir failed."
    echo " -> $test: TEST-FAIL"
    rm -rf test_dir
    exit
fi

busybox mv test_dir/test1 test_dir/test2
if [ "$(ls test_dir)" = "test2" ]
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi;
rm -rf test_dir

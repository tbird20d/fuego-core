#!/bin/sh

#  The testscript checks the following options of the command ln
#  1) Option: -s

test="ln"

mkdir test_dir
echo "Hello world." > ./test_dir/test1
busybox ln -s test1 ./test_dir/link_to_other_file
ls -l ./test_dir > log
if grep "link_to_other_file" log
then
    echo " -> $test: Sym Link to file created."
else
    echo " -> $test: Sym Link to file creation failure."
    echo " -> $test: TEST-FAIL"
    rm log
    rm -rf test_dir
    exit
fi;

echo "changed text." > ./test_dir/test1
if [ "$(cat ./test_dir/link_to_other_file)" = "changed text." ]
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi;
rm log;
rm -rf test_dir;

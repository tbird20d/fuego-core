#!/bin/sh

#  The testscript checks the following options of the command logger
#  1) Option none

test="logger"

msg="This is a test message $(date)"
busybox logger "$msg"
if grep "$msg" /var/log/syslog
then
    echo " -> $test: TEST-PASS"
else
    echo "  Didn't find test string in log"
    echo " -> $test: TEST-FAIL"
fi;

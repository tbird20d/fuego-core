#!/bin/sh

#  The testscript checks the following options of the command hexdump
#  1) Option: -c

test="hexdump"

mkdir test_dir
touch test_dir/test1
echo "HELLO WORLD" > test_dir/test1
busybox hexdump -c test_dir/test1 > log
if head -n 1 log | grep "0000000   H   E   L   L   O       W   O   R   L   D  \\\n" && [ "$(tail -n 1 log)" = "000000c" ]
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi;
rm log
rm -rf test_dir

#!/bin/sh

#  The testscript checks the following options of the command sleep
#  1) Option none

test="sleep"

if sleep 0 | ps aux | grep "[s]leep 0"
then
    echo " -> $test: sleep 0 failed."
    echo " -> $test: TEST-FAIL"
    exit
else
    echo " -> $test: sleep 0 succeeded."
fi;

if sleep 1 | ps aux | grep "[s]leep 1"
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi;

#!/bin/sh

#  The testscript checks the following options of the command wc
#  1) Option none

test="wc"

if [ "$(echo 'HELLO WORLD' | busybox wc | grep ".*1         2        12.*")" ]
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi;

#!/bin/sh

#  The testscript checks the following options of the command rmdir
#  1) Option none

test="rmdir"

mkdir test_dir
if ls -l | grep test_dir
then
    echo " -> $test: mkdir test_dir verification succeeded."
else
    echo " -> $test: mkdir test_dir verification failed."
    echo " -> $test: TEST-FAIL"
    exit
fi;

busybox rmdir test_dir
if [ "$(ls -l | grep test_dir)" = "" ]
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi;

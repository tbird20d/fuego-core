#!/bin/sh

#  The testscript checks the following options of the command test
#  1) Option: none

test="test"

if ! busybox test 1 -eq 2
then
    echo " -> $test: busybox test 1 -eq 2 verification succeeded."
else
    echo " -> $test: busybox test 1 -eq 2 verification failed."
    echo " -> $test: TEST-FAIL"
    exit
fi;

if busybox test 1 -eq 1
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi;

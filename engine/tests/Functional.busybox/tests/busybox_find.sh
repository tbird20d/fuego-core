#!/bin/sh

#  The testscript checks the following options of the command find
#  1) Option: -name

test="find"

mkdir test_find_dir
if [ "$(busybox find . -name test_find_dir)" = "./test_find_dir" ]
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi;
rm -rf test_find_dir

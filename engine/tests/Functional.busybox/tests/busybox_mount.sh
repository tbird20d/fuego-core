#!/bin/sh

#  The testscript checks the following options of the command mount
#  1) Option none

test="mount"

if busybox mount | grep "/ "
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi;

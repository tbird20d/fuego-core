#!/bin/sh

#  The testscript checks the following options of the command xargs
#  1) Option none

test="xargs"

mkdir test_dir
echo test1 > ./test_dir/test1
echo test11 > ./test_dir/test2
echo test111 > ./test_dir/test3
echo "./test_dir/test1" > testfile
echo "./test_dir/test2" >> testfile
echo "./test_dir/test3" >> testfile

cat testfile | busybox xargs wc > log

# NOTE: 'busybox xargs wc' MAY be executed as 'busybox xargs busybox wc'
# which may produce output with different whitespace, because
# 'busybox wc' does not produce the same output as coreutils 'wc'
# Use regular expressions in the following tests to ignore whitespace
# differences.

if echo "$(head -n 1 log)" | grep " *1 *1 *6 ./test_dir/test1" && \
   echo "$(head -n 2 log | tail -n 1)" | grep " *1 *1 *7 ./test_dir/test2" && \
   echo "$(tail -n 2 log | head -n 1)" | grep " *1 *1 *8 ./test_dir/test3" && \
   echo "$(tail -n 1 log)" | grep " *3 *3 *21 total" ;
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: output from 'xargs wc' for a file list was not as expected"
    echo "output was:"
    cat log
    echo " -> $test: TEST-FAIL"
fi;
rm testfile
rm log
rm -fr test_dir

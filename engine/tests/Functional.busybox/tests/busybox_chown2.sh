#!/bin/sh

#  The testscript checks the following options of the command chown
#  1) Option: -R

test="chown2"

mkdir test_dir
touch test_dir/test1
touch test_dir/test2
orig_user=$(id -n -u | cut -b -8)
orig_group=$(id -n -g | cut -b -8)
busybox ls -l ./test_dir | grep -v "total" | tr -s ' ' | cut -d' ' -f3,4,9 > log1
if [ "$(head -n 1 log1)" = "$orig_user $orig_group test1" ] && [ "$(tail -n 1 log1)" = "$orig_user $orig_group test2" ]
then
    echo " -> $test: test_dir contents verification succeeded."
else
    echo " -> $test: test_dir contents verification failed."
    echo " -> $test: TEST-FAIL"
    rm log1
    rm -rf ./test_dir
    exit
fi;

busybox chown -R bin ./test_dir
busybox ls -l ./test_dir | grep -v "total" | tr -s ' ' | cut -d' ' -f3,4,9 > log2
if [ "$(head -n 1 log2)" = "bin $orig_group test1" ] && [ "$(tail -n 1 log2)" = "bin $orig_group test2" ]
then
    echo " -> $test: group change for multiple files (-R) succeeded"
else
    echo " -> $test: group change for multiple files (-R) not found."
    echo " -> $test: TEST-FAIL"
    rm log1 log2
    rm -rf ./test_dir
    exit
fi;

busybox chown -R bin.bin ./test_dir
busybox ls -l ./test_dir | grep -v "total" | tr -s ' ' | cut -d' ' -f3,4,9 > log3
if [ "$(head -n 1 log3)" = "bin bin test1" ] && [ "$(tail -n 1 log3)" = "bin bin test2" ]
then
    echo " -> $test: user and group change for multiple files (-R) succeeded"
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: user and group change for multiple files (-R) not found"
    echo " -> $test: TEST-FAIL"
fi;
rm log1 log2 log3;
rm -rf ./test_dir;

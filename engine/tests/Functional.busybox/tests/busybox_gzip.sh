#!/bin/sh

#  The testscript checks the following options of the command gzip
#  1) Option: -cd -t -c -d

test="gzip"

mkdir test_dir
echo "This is a file to test gzip." > ./test_dir/test1
busybox gzip ./test_dir/test1
if [ "$(ls test_dir)" = "test1.gz" ]
then
    echo " -> $test: File test1.gz created."
else
    echo " -> $test: File test1.gz creation failed."
    rm -rf test_dir
    exit
fi;

if [ "$(busybox gzip -cd ./test_dir/test1.gz)" = "This is a file to test gzip." ]
then
    echo " -> $test: option -cd verification#1 succeeded."
else
    echo " -> $test: option -cd verification#1 failed."
    echo " -> $test: TEST-FAIL"
    rm -rf test_dir
    exit
fi;

if gunzip -t ./test_dir/test1.gz
then
    echo " -> $test: option -t verification#2 succeeded."
else
    echo " -> $test: option -t verification#2 failed."
    echo " -> $test: TEST-FAIL"
    rm -rf test_dir
    exit
fi;

if [ "$(gunzip -c ./test_dir/test1.gz)" = "This is a file to test gzip." ]
then
    echo " -> $test: option -c verification#3 succeeded."
else
    echo " -> $test: option -c verification#3 failed."
    echo " -> $test: TEST-FAIL"
    rm -rf test_dir
    exit
fi;

if busybox gzip -d ./test_dir/test1.gz
then
    echo " -> $test: option -d verification#4 succeeded."
else
    echo " -> $test: option -d verification#4 failed."
    echo " -> $test: TEST-FAIL"
    rm -rf test_dir
    exit
fi;

if [ "$(ls test_dir)" = "test1" ]
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi;
rm -rf test_dir

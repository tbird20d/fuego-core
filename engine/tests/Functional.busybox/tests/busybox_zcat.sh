#!/bin/sh

#  The testscript checks the following options of the command zcat
#  1) Option none

test="zcat"

rm -fr test_dir
mkdir test_dir
echo "This is a file to test zcat." > ./test_dir/test1
gzip ./test_dir/test1
if [ "$(busybox zcat ./test_dir/test1.gz)" = "This is a file to test zcat." ]
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi;
rm -fr test_dir

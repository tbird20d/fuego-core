#!/bin/sh

#  In the target start atd, add job and verify job completion.
#  Check the syslog string "fuego autotest".

test="syslog"

. ./fuego_board_function_lib.sh

set_init_manager

logger_service=$(detect_logger_service)

exec_service_on_target $logger_service stop
exec_service_on_target atd stop

if [ -f /var/log/syslog ]
then
    mv /var/log/syslog /var/log/syslog_bak
fi
exec_service_on_target $logger_service restart

if exec_service_on_target atd start
then
    echo " -> start atd succeeded."
else
    echo " -> start atd failed."
fi

mkdir test_dir
cp data/test_add.sh test_dir/

at -f test_dir/test_add.sh now + 1 minutes

echo "sleep 60s!"
sleep 60

if cat /var/log/syslog | grep "hello fuego autotest"
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi

rm -fr test_dir

at -d $(at -l | cut -b 1-2)
exec_service_on_target atd stop
exec_service_on_target $logger_service stop

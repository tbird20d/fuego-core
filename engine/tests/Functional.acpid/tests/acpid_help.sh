#!/bin/sh

#  In target, run comannd acpi_listen.
#  To make sure that there is string "Usage" in output.

test="help"

if acpi_listen -h | grep Usage
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi;

#!/bin/sh

# If the target start the acpid.
# Add the action file and event file to /etc/acpid to reload acpid.
# Check the log file.

test="reload"

. ./fuego_board_function_lib.sh

set_init_manager
logger_service=$(detect_logger_service)

if [ ! -f /etc/acpi/actions ]
then
    mkdir -p /etc/acpi/actions
fi

if [ ! -f /etc/acpi/events/testhkey ]
then
    touch /etc/acpi/events/testhkey
fi

if [ ! -f /etc/acpi/actions/test_action.sh ]
then
    touch /etc/acpi/actions/test_action.sh
fi

cp data/testhkey /etc/acpi/events/testhkey
cp data/test_action.sh /etc/acpi/actions/test_action.sh

chmod +x /etc/acpi/actions/test_action.sh
if [ -f /var/log/acpid ]
then
    rm -f /var/log/acpid
fi

exec_service_on_target $logger_service stop
if [ -f /var/log/syslog ]
then
    mv /var/log/syslog /var/log/syslog_bak
fi

sleep 5

exec_service_on_target $logger_service restart

if exec_service_on_target acpid restart
then
    echo " -> restart of acpid succeeded."
else
    echo " -> restart of acpid failed."
    echo " -> $test: TEST-FAIL"
    exit
fi

if tail /var/log/syslog | grep "starting up"
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi
exec_service_on_target acpid stop
rm -f /etc/acpi/events/testhkey
rm -f /etc/acpi/actions/test_action.sh
if [ -e /var/log/syslog_bak ]
then
    mv /var/log/syslog_bak /var/log/syslog
fi

# this function sleeps for the time specified by the
# spec (default=300 seconds, or 5 minutes)

function test_pre_check {
    assert_define FUNCTIONAL_FUEGO_SLEEP_TIME
}

function test_run {
    ST=$FUNCTIONAL_FUEGO_SLEEP_TIME
    log_this "echo starting sleeps for $ST seconds"

    # start with some simple stuff
    for i in $(seq $ST) ; do
        echo -n "."
        sleep 1
        if [ $(( $i % 30 )) = "0" ] ; then
           echo
           log_this "echo slept for $i seconds"
        fi
    done
    echo

    log_this "echo DONE!"
}

function test_processing {
    log_compare "$TESTDIR" "1" "DONE" "p"
}

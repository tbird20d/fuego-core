#!/bin/sh

#  In the target start dovecot, and confirm the process condition by command ps.
#  check the keyword "dovecot".

test="ps"

. "./fuego_board_function_lib.sh"

set_init_manager

exec_service_on_target dovecot stop
if exec_service_on_target dovecot start
then
    echo " -> start of dovecot succeeded."
else
    echo " -> start of dovecot failed."
    echo " -> $test: TEST-FAIL"
    exit
fi

if ps -N a | grep [d]ovecot
then
    echo " -> get the pid of dovecot."
else
    echo " -> can't get the pid of dovecot."
    echo " -> $test: TEST-FAIL"
    exec_service_on_target dovecot stop
    exit
fi

if exec_service_on_target dovecot stop
then
    echo " -> stop of dovecot succeeded."
else
    echo " -> stop of dovecot failed."
    echo " -> $test: TEST-FAIL"
    exit
fi

if ps -N a | grep [d]ovecot
then
    echo " -> $test: TEST-FAIL"
else
    echo " -> $test: TEST-PASS"
fi

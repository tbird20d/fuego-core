#!/bin/sh

#  In the target to start dovecot dovecot, to confirm the acquisition of the log.
#  check the file master.pid.

test="pidfile"

. "./fuego_board_function_lib.sh"

set_init_manager

exec_service_on_target dovecot stop

if exec_service_on_target dovecot start
then
    echo " -> start of dovecot succeeded."
else
    echo " -> start of dovecot failed."
    echo " -> $test: TEST-FAIL"
fi

sleep 5

if ls /var/run/dovecot/master.pid
then
    echo " -> ls /var/run/dovecot/master.pid succeeded."
else
    echo " -> ls /var/run/dovecot/master.pid failed."
    echo " -> $test: TEST-FAIL"
    exec_service_on_target dovecot stop
    exit
fi

exec_service_on_target dovecot stop

if test ! -f /var/run/dovecot/master.pid
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi

function test_run {
    if [ -f /fuego-rw/boards/ftc-test.vars ] ; then
        rm /fuego-rw/boards/ftc-test.vars
    fi
    log_this "clitest --prefix tab $TEST_HOME/ftc-tests"
}

function test_processing {
    # log_compare "$TESTDIR" "1" "^OK: 30 of 30" "p"
    log_compare "$TESTDIR" "0" "^[[]FAILED " "0"
}

#!/bin/sh

#  Run the rndc-confgen command in the chroot environment to find the rndc.key file.

test="chroot_rndc-confgen"

kill -9 $(pgrep named)

if [ ! -d /var/named/chroot/etc/bind ]
then
    mkdir -p /var/named/chroot/etc/bind
fi

if [ ! $(ls /var/named/chroot/etc/bind/) ]
then
    cp -f /etc/bind/* /var/named/chroot/etc/bind/
fi

if [ ! -d /var/named/chroot/var/named ]
then
    mkdir -p /var/named/chroot/var/named
fi

if [ ! -d /var/named/chroot/var/cache/bind ]
then
    mkdir -p /var/named/chroot/var/cache/bind
fi

if [ ! -d /var/named/chroot/var/run/named ]
then
    mkdir -p /var/named/chroot/var/run/named
fi

cp /etc/sysconfig/named /etc/sysconfig/named_bak
cp data/bind9/sysconfig/named /etc/sysconfig/named

if [ -f /etc/bind/rndc.key ]
then
    mv /etc/bind/rndc.key /etc/bind/rndc.key_bak
fi

if [ -f /var/named/chroot/etc/bind/rndc.key ]
then
    rm /var/named/chroot/etc/bind/rndc.key
fi

rndc-confgen -a -k rndckey -t /var/named/chroot
if ls /var/named/chroot/etc/bind/rndc.key
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi

rm -rf /var/named
mv /etc/sysconfig/named_bak /etc/sysconfig/named
mv /etc/bind/rndc.key_bak /etc/bind/rndc.key

#!/bin/sh

#  After running the /etc/named.conf to run to the target, run the named checkconf command and verify the normal end.

test="named_chkconf"

. ./fuego_board_function_lib.sh

set_init_manager

exec_service_on_target named stop

if [ -f /etc/bind/named.conf ]
then
    cp /etc/bind/named.conf /etc/bind/named.conf_bak
fi

cp data/bind9/named.conf /etc/bind/named.conf

if named-checkconf
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi

if [ -f /etc/bind/named.conf_bak ]
then
    mv /etc/bind/named.conf_bak /etc/bind/named.conf
fi

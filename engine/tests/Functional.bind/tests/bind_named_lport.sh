#!/bin/sh

# Start the named on target.
# Check the listening port.

test="named_lport"

. ./fuego_board_function_lib.sh

set_init_manager

if [ -f /etc/bind/rndc.key ]
then
    rm -f /etc/bind/rndc.key
fi

exec_service_on_target named stop

if exec_service_on_target named start
then
    echo " -> start of named succeeded."
else
    echo " -> start of named failed."
    echo " -> $test: TEST-FAIL"
    exit
fi

if netstat -ln | grep ":53"
then
    echo " -> port 53 is exist."
else
    echo " -> port 53 is not exist."
    echo " -> $test: TEST-FAIL"
    exec_service_on_target named stop
    exit
fi

if netstat -ln | grep ":953"
then
    echo " -> $test: TEST-PASS"
else
    echo " -> port 953 is not exist."
    echo " -> $test: TEST-FAIL"
fi

exec_service_on_target named stop

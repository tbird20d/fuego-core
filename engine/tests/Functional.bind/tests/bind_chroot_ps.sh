#!/bin/sh

#  In the target start named chroot, and confirm the process condition by command ps.

test="chroot_ps"

kill -9 $(pgrep named)

if [ ! -d /var/named/chroot/etc/bind ]
then
    mkdir -p /var/named/chroot/etc/bind
fi

if [ ! $(ls /var/named/chroot/etc/bind/) ]
then
    cp -f /etc/bind/* /var/named/chroot/etc/bind/
fi

if [ ! -d /var/named/chroot/var/named ]
then
    mkdir -p /var/named/chroot/var/named
fi

if [ ! -d /var/named/chroot/var/cache/bind ]
then
    mkdir -p /var/named/chroot/var/cache/bind
fi

if [ ! -d /var/named/chroot/var/run/named ]
then
    mkdir -p /var/named/chroot/var/run/named
fi

named -t /var/named/chroot

if ps aux | grep "[/]var/named/chroot"
then
    echo " -> get the process of named."
    echo " -> $test: TEST-PASS"
else
    echo " -> can't get the process of named."
    echo " -> $test: TEST-FAIL"
fi
kill -9 $(pgrep named)
rm -rf /var/named

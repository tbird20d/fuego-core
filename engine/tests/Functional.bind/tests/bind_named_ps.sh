#!/bin/sh

#  In the target start bind, and confirm the process condition by command ps.

test="named_ps"

. ./fuego_board_function_lib.sh

set_init_manager

exec_service_on_target named stop

if exec_service_on_target named start
then
    echo " -> start of named succeeded."
else
    echo " -> start of named failed."
    echo " -> $test: TEST-FAIL"
    exit
fi

sleep 5

if ps aux | grep "[/]usr/sbin/named"
then
    echo " -> get the process of named."
else
    echo " -> can't get the process of named."
    echo " -> $test: TEST-FAIL"
    exec_service_on_target named stop
    exit
fi

exec_service_on_target named stop

if ps aux | grep "[/]usr/sbin/named"
then
    echo " -> $test: TEST-FAIL"
else
    echo " -> $test: TEST-PASS"
fi

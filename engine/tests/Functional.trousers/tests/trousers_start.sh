#!/bin/sh

#  In target, start the service tcsd.

test="start"

. ./fuego_board_function_lib.sh

set_init_manager

exec_service_on_target tcsd stop

if exec_service_on_target tcsd start
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi

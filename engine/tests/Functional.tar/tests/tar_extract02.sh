#!/bin/sh

#  In target, run command tar.
#  option: xzf(gzip)

test="extract02"

if tar xzf data/test.tar.gz
then
    echo " -> tar xzf succeeded."
else
    echo " -> tar xzf failed."
fi

if test -f test/data/test_b.txt
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi;
rm -fr test

#!/bin/sh

#  In target, run command tar.
#  option: tzf(gzip)

test="list02"

if tar tzf data/test.tar.gz | grep "test_b.txt"
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi;

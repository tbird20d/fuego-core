#!/bin/sh

#  In the target start bgpd and zebra, then confirm the process condition by /var/run/quagga/bgpd.pid file.
#  check the keyword "bgpd".

test="pidfile"

. ./fuego_board_function_lib.sh

set_init_manager

exec_service_on_target bgpd stop
exec_service_on_target zebra stop
rm -f /var/run/quagga/bgpd.pid

#Backup the config file
mv /etc/quagga/bgpd.conf /etc/quagga/bgpd.conf.bck
mv /etc/quagga/zebra.conf /etc/quagga/zebra.conf.bck

cp data/bgpd.conf /etc/quagga/bgpd.conf
cp data/zebra.conf /etc/quagga/zebra.conf
chown quagga:quagga /etc/quagga/*.conf

if exec_service_on_target zebra start
then
    echo " -> start of zebra succeeded."
else
    echo " -> start of zebra failed."
    echo " -> $test: TEST-FAIL"
    exit
fi

if exec_service_on_target bgpd start
then
    echo " -> start of bgpd succeeded."
else
    echo " -> start of bgpd failed."
    echo " -> $test: TEST-FAIL"
    exit
fi

if ls /var/run/quagga/bgpd.pid
then
    echo " -> get the pid of bgpd."
else
    echo " -> can't get the pid of bgpd."
    echo " -> $test: TEST-FAIL"
    exit
fi

exec_service_on_target bgpd stop
exec_service_on_target zebra stop

if ls /var/run/quagga/bgpd.pid
then
    echo " -> $test: TEST-FAIL"
else
    echo " -> $test: TEST-PASS"
fi

#Restore the config file
mv /etc/quagga/bgpd.conf.bck /etc/quagga/bgpd.conf
mv /etc/quagga/zebra.conf.bck /etc/quagga/zebra.conf

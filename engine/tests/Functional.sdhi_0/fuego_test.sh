#
# user must define in the board file:
#  SDHI_DEV_NAME = the name of SD/MMC device in /dev/disk/by-path/
#  SDHI_BLOCK_NUM = which block of SD/MMC device used for test
#  SDHI_DRIVER_NAME = the name of SD/MMC driver in /sys/bus/platform/drivers/
#

function test_pre_check {
    is_on_target_path dd PROGRAM_DD
    assert_define SDHI_DEV_NAME "Please define in board file as the name of sd/mmc device from /dev/disk/by-path/"
    assert_define SDHI_BLOCK_NUM "Please define in board file as the block number of sd/mmc device"
    assert_define SDHI_DRIVER_NAME "Please define in board file as the name of sd/mmc driver from /sys/bus/platform/drivers/"

}

function test_deploy {
    put $TEST_HOME/check_sdhi.sh $BOARD_TESTDIR/fuego.$TESTDIR/
}

function test_run {
    report "$BOARD_TESTDIR/fuego.$TESTDIR/check_sdhi.sh \"$SDHI_DEV_NAME\" \"$SDHI_BLOCK_NUM\" \"$SDHI_DRIVER_NAME\""
}

function test_processing {
    log_compare "$TESTDIR" 0 "^not ok" "n"
}

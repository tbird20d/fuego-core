#!/usr/bin/python

import os, re, sys

sys.path.insert(0, os.environ['FUEGO_CORE'] + '/engine/scripts/parser')
import common as plib


regex_string = "^(Time:)(\ *)([\d]{1,8}.?[\d]{1,3})(.*)$"

measurements = {}
matches = plib.parse_log(regex_string)

if matches:
    measurements['default.Hackbench'] = [{"name": "Time", "measure" : float(matches[0][2])}]

sys.exit(plib.process(measurements))

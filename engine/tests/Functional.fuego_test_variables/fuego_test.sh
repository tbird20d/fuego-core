# test that precedence of variables during a test run is correct
# correct (expected) precedence is:
#
# environment var= lowest precedence (from jenkins, ftc)
# common.sh vars
# overlay class (base-board, base-distrib, base-funcs, base-params, base-vars)
# overlay override (distrib, board, stored ($board.var))
# spec vars (from spec.json)
# dynamic vars (from ftc command line, through custom spec.json)
# core vars (from main.sh and functions.sh)
# fuego_test vars (from fuego_test.sh)= highest precedence
#
# this test should be run on board "fuego-test"
#

NEED_MEMORY=10K

# $tmpfile = file where environment data was stored
# $1 = testcase info (number and description)
# $2 = var name
# $3 = expected value
function check_var {
        tc_info="$1"
        if grep $2 $tmpfile | grep -q "$3" ; then
            log_this "echo \"ok $tc_info\""
        else
            log_this "echo \"not ok $tc_info\""
        fi
}

function test_run {
    if [ "$TESTSPEC" = "default" ] ; then
        # work around a bug with nested invocations.  The nested calls
        # to 'ftc run-test' will wipe out the 'before' system log.
        # Save it off before that happens, and restore it after
        # the nested tests.
        local fuego_test_tmp="${FUEGO_TARGET_TMP:-/tmp}/fuego.$TESTDIR"
        local syslog_before="${fuego_test_tmp}/${NODE_NAME}.${BUILD_ID}.${BUILD_NUMBER}.before"

        # NOTE: this approach doesn't handle test concurrency very well
        # multiple tests running nested could overwrite syslog_before.save
        # But I don't care at the moment.
        cmd "cp ${syslog_before} /tmp/syslog_before.save"

        # do setup for the test, and call it in nested fashion
        export FTV_VAR_01="environment one"
        export FTV_VAR_02="environment two"
        export FTV_VAR_03="environment three"
        export FTV_VAR_04="environment four"
        export FTV_VAR_05="environment five"
        export FTV_VAR_06="environment six"
        export FTV_VAR_07="environment seven"
        export FTV_VAR_08="environment eight"
        export FTV_VAR_09="environment nine"
        export FTV_VAR_10="environment ten"
        export FTV_VAR_11="environment eleven"
        export FTV_VAR_12="environment twelve"
        export FTV_VAR_13="environment thirteen"
        export FTV_VAR_14="environment fourteen"
        export FTV_VAR_15="environment fifteen"
        export FTV_VAR_16="environment sixteen"
        export FTV_VAR_17="environment seveteen"
        export FTV_VAR_18="environment eighteen"
        export FTV_VAR_19="environment nineteen"
        export FTV_VAR_20="environment twenty"
        export FTV_VAR_21="environment twenty-one"
        export FTV_VAR_22="environment twenty-two"
        export FTV_VAR_23="environment twenty-three"
        export FTV_VAR_24="environment twenty-four"
        export FTV_VAR_25="environment twenty-five"
        export FTV_VAR_26="environment twenty-six"
        export FTV_VAR_27="environment twenty-seven"
        export FTV_VAR_28="environment twenty-eight"
        export FTV_VAR_29="environment twenty-nine"
        export FUNCTIONAL_FUEGO_TEST_VARIABLES_VAR_01="environment one"
        export FUNCTIONAL_FUEGO_TEST_VARIABLES_VAR_02="environment two"
        export FUNCTIONAL_FUEGO_TEST_VARIABLES_VAR_03="environment three"
        export FUNCTIONAL_FUEGO_TEST_VARIABLES_VAR_04="environment four"
        export FUNCTIONAL_FUEGO_TEST_VARIABLES_VAR_05="environment five"
        export FUNCTIONAL_FUEGO_TEST_VARIABLES_VAR_06="environment six"
        export FUNCTIONAL_FUEGO_TEST_VARIABLES_VAR_07="environment seven"
        export FUNCTIONAL_FUEGO_TEST_VARIABLES_VAR_08="environment eight"
        export FUNCTIONAL_FUEGO_TEST_VARIABLES_VAR_09="environment nine"
        export FUNCTIONAL_FUEGO_TEST_VARIABLES_VAR_10="environment ten"
        export FUNCTIONAL_FUEGO_TEST_VARIABLES_VAR_11="environment eleven"
        export FUNCTIONAL_FUEGO_TEST_VARIABLES_VAR_12="environment twelve"
        export FUNCTIONAL_FUEGO_TEST_VARIABLES_VAR_13="environment thirteen"
        export FUNCTIONAL_FUEGO_TEST_VARIABLES_VAR_14="environment fourteen"
        export FUNCTIONAL_FUEGO_TEST_VARIABLES_VAR_15="environment fifteen"
        export FUNCTIONAL_FUEGO_TEST_VARIABLES_VAR_16="environment sixteen"
        export FUNCTIONAL_FUEGO_TEST_VARIABLES_VAR_17="environment seventeen"
        export FUNCTIONAL_FUEGO_TEST_VARIABLES_VAR_18="environment eighteen"
        export FUNCTIONAL_FUEGO_TEST_VARIABLES_VAR_19="environment nineteen"
        export FUNCTIONAL_FUEGO_TEST_VARIABLES_VAR_20="environment twenty"
        export FUNCTIONAL_FUEGO_TEST_VARIABLES_VAR_21="environment twenty-one"
        export FUNCTIONAL_FUEGO_TEST_VARIABLES_VAR_22="environment twenty-two"
        export FUNCTIONAL_FUEGO_TEST_VARIABLES_VAR_23="environment twenty-three"
        export FUNCTIONAL_FUEGO_TEST_VARIABLES_VAR_24="environment twenty-four"
        export FUNCTIONAL_FUEGO_TEST_VARIABLES_VAR_25="environment twenty-five"
        export FUNCTIONAL_FUEGO_TEST_VARIABLES_VAR_26="environment twenty-six"
        export FUNCTIONAL_FUEGO_TEST_VARIABLES_VAR_27="environment twenty-seven"
        export FUNCTIONAL_FUEGO_TEST_VARIABLES_VAR_28="environment twenty-eight"
        export FUNCTIONAL_FUEGO_TEST_VARIABLES_VAR_29="environment twenty-nine"

        # prepare stored variables
        ftc set-var -b $NODE_NAME "FTV_VAR_04=stored four"
        ftc set-var -b $NODE_NAME "FTV_VAR_10=stored ten"
        ftc set-var -b $NODE_NAME "FTV_VAR_15=stored fifteen"
        ftc set-var -b $NODE_NAME "FTV_VAR_20=stored twenty"
        ftc set-var -b $NODE_NAME "FTV_VAR_21=stored twenty-one"
        ftc set-var -b $NODE_NAME "FTV_VAR_22=stored twenty-two"
        ftc set-var -b $NODE_NAME "FTV_VAR_23=stored twenty-three"

        ftc set-var -b $NODE_NAME "FUNCTIONAL_FUEGO_TEST_VARIABLES_VAR_21=stored twenty-one"
        ftc set-var -b $NODE_NAME "FUNCTIONAL_FUEGO_TEST_VARIABLES_VAR_22=stored twenty-two"

        export FUEGO_CALLER=nested
        # specify dynamic vars on ftc command line
        set +e
        ftc run-test -b $NODE_NAME -t $TESTDIR -s "doit" \
            --dynamic-vars "{'VAR_06':'dynamic six', \
                'VAR_12':'dynamic twelve', \
                'VAR_17':'dynamic seventeen', \
                'VAR_21':'dynamic twenty-one', \
                'VAR_24':'dynamic twenty-four', \
                'VAR_27':'dynamic twenty-seven', \
                'VAR_28':'dynamic twenty-eight'}" \
                ; FTV_RET_CODE=$?
        set -e

        echo "Back from 'ftc run-test' with spec 'doit'"

        # get last file created for run of this test with 'doit' spec
        # that should correspond to the one just completed above
        sublog_filename=$(ls $FUEGO_RW/logs/$TESTDIR/$NODE_NAME.doit.*/testlog.txt | \
            sort -t. -k 4 -n | tail -n 1)
        echo "sublog_filename=$sublog_filename"
        log_this "cat $sublog_filename"

        # re-create target log dir, and restore the 'before' syslog dump
        cmd "mkdir -p ${fuego_test_tmp}"
        cmd "mv -f /tmp/syslog_before.save ${syslog_before}"
    fi

    if [ "$TESTSPEC" = "doit" ] ; then
        export tmpfile=$(mktemp)

        # set local 'fuego_test' vars needed for test
        FTV_VAR_08="fuego_test eight"
        FTV_VAR_14="fuego_test fourteen"
        FTV_VAR_19="fuego_test nineteen"
        FTV_VAR_23="fuego_test twenty-three"
        FUNCTIONAL_FUEGO_TEST_VARIABLES_VAR_26="fuego_test twenty-six"
        FUNCTIONAL_FUEGO_TEST_VARIABLES_VAR_28="fuego_test twenty-eight"
        FTV_VAR_29="fuego_test twenty-nine"

        FTV_VAR_SNAPSHOT_FILENAME="$SNAPSHOT_FILENAME"

        iprint "Checking FTV test variables"
        env | grep [SV]_VAR_ | sort >$tmpfile
        log_this "echo 1..29"
        log_this "echo -e \"FTV test variables:\" ; cat $tmpfile"

        # actually perform the variable checks
        # now do test cases:
        # 1. env var shows up in test environment
        check_var "1 - env var shows up in test environment" \
            FTV_VAR_01 "environment one"

        ####   env vars overrides
        check_var "2 - precedence of class vars over env vars" \
            FTV_VAR_02 "class two"

        check_var "3 - precedence of board vars over env vars" \
            FTV_VAR_03 "board three"

        check_var "4 - precedence of stored vars over env vars" \
            FTV_VAR_04 "stored four"

        check_var "5 - precedence of spec vars over env vars" \
            FUNCTIONAL_FUEGO_TEST_VARIABLES_VAR_05 "spec five"

        check_var "6 - precedence of dynamic vars over env vars" \
            FUNCTIONAL_FUEGO_TEST_VARIABLES_VAR_06 "dynamic six"

        check_var "7 - precedence of core vars over env vars" \
            FTV_VAR_07 "core seven"

        check_var "8 - precedence of fuego_test vars over env vars" \
            FTV_VAR_08 "fuego_test eight"

        #   class vars overrides
        check_var "9 - precedence of board vars over class vars" \
            FTV_VAR_09 "board nine"

        check_var "10 - precedence of stored vars over class vars" \
            FTV_VAR_10 "stored ten"

        check_var "11 - precedence of spec vars over class vars" \
            FUNCTIONAL_FUEGO_TEST_VARIABLES_VAR_11 "spec eleven"

        check_var "12 - precedence of dynamic vars over class vars" \
            FUNCTIONAL_FUEGO_TEST_VARIABLES_VAR_12 "dynamic twelve"

        check_var "13 - precedence of core vars over class vars" \
            FTV_VAR_13 "core thirteen"

        check_var "14 - precedence of fuego_test vars over class vars" \
            FTV_VAR_14 "fuego_test fourteen"

        #   board vars overrides
        check_var "15 - precedence of stored vars over board vars" \
            FTV_VAR_15 "stored fifteen"

        check_var "16 - precedence of spec vars over board vars" \
            FUNCTIONAL_FUEGO_TEST_VARIABLES_VAR_16 "spec sixteen"

        check_var "17 - precedence of dynamic vars over board vars" \
            FUNCTIONAL_FUEGO_TEST_VARIABLES_VAR_17 "dynamic seventeen"

        check_var "18 - precedence of core vars over board vars" \
            FTV_VAR_18 "core eighteen"

        check_var "19 - precedence of fuego_test vars over board vars" \
            FTV_VAR_19 "fuego_test nineteen"

        #   stored vars overrides
        check_var "20 - precedence of spec vars over stored vars" \
            FUNCTIONAL_FUEGO_TEST_VARIABLES_VAR_20 "spec twenty"

        check_var "21 - precedence of dynamic vars over stored vars" \
            FUNCTIONAL_FUEGO_TEST_VARIABLES_VAR_21 "dynamic twenty-one"

        check_var "22 - precedence of core vars over stored vars" \
            FTV_VAR_22 "core twenty-two"

        check_var "23 - precedence of fuego_test vars over stored vars" \
            FTV_VAR_23 "fuego_test twenty-three"

        #   spec vars overrides
        check_var "24 - precedence of dynamic vars over spec vars" \
            FUNCTIONAL_FUEGO_TEST_VARIABLES_VAR_24 "dynamic twenty-four"

        check_var "25 - precedence of core vars over spec vars" \
            FUNCTIONAL_FUEGO_TEST_VARIABLES_VAR_25 "core twenty-five"

        check_var "26 - precedence of fuego_test vars over spec vars" \
            FUNCTIONAL_FUEGO_TEST_VARIABLES_VAR_26 "fuego_test twenty-six"

        #   dynamic vars overrides
        check_var "27 - precedence of core vars over dynamic vars" \
            FUNCTIONAL_FUEGO_TEST_VARIABLES_VAR_27 "core twenty-seven"

        check_var "28 - precedence of fuego_test vars over dynamic vars" \
            FUNCTIONAL_FUEGO_TEST_VARIABLES_VAR_28 "fuego_test twenty-eight"

        #   core vars overrides
        check_var "29 - precedence of fuego_test vars over core vars" \
            FTV_VAR_29 "fuego_test twenty-nine"
    fi
}

function test_processing {
    log_compare "$TESTDIR" "0" "not ok" "n"
}

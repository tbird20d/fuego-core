#
# scifab test: test of the 'serial communication interface' interrupt
#
# The user MUST define in the board file:
#  SCIFAB_NAME = the partial name in /proc/interrupts for the sci
#   (serial communication interface) interrupt
#
# Optionally, the user MAY define in the board file:
#  SCIFAB_PATTERN = a search pattern used for matching initialization
#    information of the sci in dmesg output
#  SCIFAB_COUNT = the matching count of SCIFAB_PATTERN in dmesg
#

function test_pre_check {
    assert_define SCI_NAME "Please define in board file as partial name of (serial communication interface (sci) interrupt"
}

function test_deploy {
    put $TEST_HOME/check_scifab.sh $BOARD_TESTDIR/fuego.$TESTDIR/
}

function test_run {
    report "$BOARD_TESTDIR/fuego.$TESTDIR/check_scifab.sh \
        \"$SCI_NAME\" \"$SCI_PATTERN\" \"$SCI_PATTERN_COUNT\""
}

function test_processing {
    log_compare "$TESTDIR" 0 "^not ok" "n"
}

#!/bin/sh

#  In target, run command mcelog.
#  Verify that the error log is decoded, and check the output.

test="mcelog_02"

mkdir mcelog
touch mcelog/mcelog_hw_error.log
cat >> mcelog/mcelog_hw_error.log <<EOF
PU 0: Machine Check Exception:                4 Bank 4: b200000000070f0f
TSC 3151881f80cc
Kernel panic - not syncing: Machine check
EOF

if mcelog --ascii < mcelog/mcelog_hw_error.log | grep -z ".*Hardware event.*MCGSTATUS.*"
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi;
rm -fr mcelog
